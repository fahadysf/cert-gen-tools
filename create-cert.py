#!/usr/bin/env python3

import os

# Setup the Certificate Parameters
CERTIFICATE_INFO = {
    'C': 'SA',
    'ST': 'Riyadh',
    'O': 'FY Home Lab',
    'OU': 'Infrastructure',
    'CN': 'docker-host.fy.loc',
    'SAN_DOMAINS': [
        'docker-host.fy.loc',
        'docker-host',
        'virtual-container-host'
    ],
    'SAN_IPS': [
        '192.168.100.102',
    ],
}

CA_CRT_FILE = 'fy-step-intermediate-ca.pem'
CA_KEY_FILE = 'fy-step-intermediate-ca.key'

CERT_KEY_FILE = CERTIFICATE_INFO['CN']+'.key'
CERT_CSR_FILE = CERTIFICATE_INFO['CN']+'.csr'
CERT_FILE = CERTIFICATE_INFO['CN']+'.pem'

CERT_KEY_SIZE = "2048"
CERT_DURATION = '730'

CA_PFX_FILE = CA_CRT_FILE+".pfx"
CERT_PFX_FILE = CERT_FILE+".pfx"
CERT_PFX_PASS = 'fc82kre'

# Generate a Server Certificate signed by above CA
# Step-1 Generate a key for this certificate
os.system(("openssl genrsa -out {0} {1}").format(CERT_KEY_FILE, CERT_KEY_SIZE))

# Step-2 Generate a CSR to be signed by the CA created above
# Note: This requires BASH shell or equivalent.
print(("Generating CSR File for {0}").format(CERTIFICATE_INFO['CN']))

opensslcnfstring = ("""
[ req ]
prompt = no
distinguished_name = req_distinguished_name
req_extensions = SAN

[ req_distinguished_name ]
C = {}
ST = {}
L = {}
O = {}
CN = {}

""").format(
    CERTIFICATE_INFO['C'],
    CERTIFICATE_INFO['ST'],
    CERTIFICATE_INFO['O'],
    CERTIFICATE_INFO['OU'],
    CERTIFICATE_INFO['CN']
)

sanstring = "[SAN]\nsubjectAltName="
for dns in CERTIFICATE_INFO['SAN_DOMAINS']:
    sanstring += "DNS:"+dns+","
for ip in CERTIFICATE_INFO['SAN_IPS']:
    sanstring += "IP:"+ip+","
# Remove the trailing comma
sanstring = sanstring[:-1]
with open('csrconfig.cnf', 'w') as fd:
    fd.write(opensslcnfstring + sanstring)
with open('sanfile.cnf', 'w') as sanfd:
    sanfd.write(sanstring)
os.system(
    ('openssl req -new -key {0} -nodes -subj "/C={1}/ST={2}/O={3}/OU={4}/CN={5}" -config csrconfig.cnf -out {6}').format(
        CERT_KEY_FILE,
        CERTIFICATE_INFO['C'],
        CERTIFICATE_INFO['ST'],
        CERTIFICATE_INFO['O'],
        CERTIFICATE_INFO['OU'],
        CERTIFICATE_INFO['CN'],
        CERT_CSR_FILE,
    )
)

# Step-3 Sign the Certificate based on the CSR and save it.
print(("Signing Cert for {0}").format(CERTIFICATE_INFO['CN']))

os.system(('openssl x509 -req -extensions SAN -extfile sanfile.cnf -in {0} -CA {1} -CAkey {2} -CAcreateserial -out {3} -days {4} -sha512').format(
    CERT_CSR_FILE,
    CA_CRT_FILE,
    CA_KEY_FILE,
    CERT_FILE,
    CERT_DURATION,
)
)


# Package the CA and Server Certs as PKCS12(.pfx) files
# Export the CA PFX
"""
print("Generating CA PFX File: "+CA_PFX_FILE)
os.system(('openssl pkcs12 -export -out {0} -inkey {1} -in {2} -passout pass:{3}').format(
    CA_PFX_FILE,
    CA_KEY_FILE,
    CA_CRT_FILE,
    CERT_PFX_PASS,
)
)"""

# Export the Server Cert PFX
print("Generating Server Cert PFX File: "+CERT_PFX_FILE)
os.system(
    ('openssl pkcs12 -export -out {0} -inkey {1} -in {2} -certfile {3} -passout pass:{4}').format(
        CERT_PFX_FILE,
        CERT_KEY_FILE,
        CERT_FILE,
        CA_CRT_FILE,
        CERT_PFX_PASS,
    )
)
