from http.server import HTTPServer, BaseHTTPRequestHandler
import ssl

KEY = './cert.key'
CERT = './cert.crt'

httpd = HTTPServer(('localhost', 4443), BaseHTTPRequestHandler)
httpd.socket = ssl.wrap_socket(httpd.socket,
                               keyfile=KEY,
                               certfile=CERT, server_side=True)

httpd.serve_forever()
